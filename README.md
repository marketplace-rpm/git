# Information / Информация

SPEC-файл для создания RPM-пакета **git**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/git`.
2. Установить пакет: `dnf install git`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)